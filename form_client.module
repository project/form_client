<?php

/**
 * @file
 * Interacts with forms provided by the form_service module.
 */

/**
 * Implementation of hook_menu().
 */
function form_client_menu() {
  $items = array();
  $items['form-client/%'] = array(
    'title' => 'Form client test',
    'page callback' => 'form_client_get_form',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Loads information about the service.
 */
function form_client_service_load() {
  $service = array(
    'url' => url('http://services.vkareh.net/services/xmlrpc'),
  );
  return $service;
}

/**
 * Retrieves a form from a constructor function, or from the cache if the form
 * was built in a previous page-load. The form is then passed on for
 * processing, after and rendered for display if necessary.
 */
function form_client_get_form($form_id) {
  $form_state = array(
    'storage' => NULL,
    'submitted' => FALSE,
  );

  $args = func_get_args();

  $form_state['post'] = $_POST;
  // Use a copy of the function's arguments for manipulation
  $args_temp = $args;
  $args_temp[0] = &$form_state;
  array_unshift($args_temp, $form_id);

  $form = call_user_func_array('form_client_retrieve_form', $args_temp);
  $form_build_id = 'form-' . md5(uniqid(mt_rand(), TRUE));
  $form['#build_id'] = $form_build_id;
  $form['#action'] = request_uri();

  // Allow both the service and the client to help prepare the form
  //drupal_prepare_form($form_id, $form, $form_state);
  form_client_prepare_form($form_id, $form, $form_state);

  unset($form_state['post']);
  $form['#post'] = $_POST;
  $form['#validate'] = array('form_client_validate_form');
  $form['#submit'] = array('form_client_submit_form');

  // Now that we know we have a form, we'll process it (validating,
  // submitting, and handling the results returned by its submission
  // handlers. Submit handlers accumulate data in the form_state by
  // altering the $form_state variable, which is passed into them by
  // reference.
  //form_client_process_form($form_id, $form, $form_state);
  drupal_process_form($form_id, $form, $form_state);

  // Most simple, single-step forms will be finished by this point --
  // drupal_process_form() usually redirects to another page (or to
  // a 'fresh' copy of the form) once processing is complete. If one
  // of the form's handlers has set $form_state['redirect'] to FALSE,
  // the form will simply be re-rendered with the values still in its
  // fields.

  // If we haven't redirected to a new location by now, we want to
  // render whatever form array is currently in hand.
  return drupal_render_form($form_id, $form);
}

/**
 * Retrieves the structured array that defines a given form.
 */
function form_client_retrieve_form($form_id, $form_state) {
  $args = func_get_args();
  $service = form_client_service_load();
  array_unshift($args, $service['url'], 'form.retrieve');
  $response = call_user_func_array('xmlrpc', $args);
  return $response;
}

/**
 * Calls the form prepare service.
 */
function form_client_prepare_form($form_id, &$form, &$form_state) {
  $service = form_client_service_load();
  $response = xmlrpc($service['url'], 'form.prepare', $form_id, $form, $form_state);
  $form = $response['form'];
  $form_state = $response['form_state'];
}

/**
 * Calls the form process service.
 */
function form_client_process_form($form_id, &$form, &$form_state) {
  $service = form_client_service_load();
  $response = xmlrpc($service['url'], 'form.process', $form_id, $form, $form_state);
  $form = $response['form'];
  $form_state = $response['form_state'];
}

/**
 * Calls the form validation service.
 */
function form_client_validate_form($form, &$form_state) {
  $service = form_client_service_load();
  $form_id = $form_state['values']['form_id'];
  $response = xmlrpc($service['url'], 'form.execute', $form_id, $form_state);
  $form_state = $response['form_state'];
  $errors = $response['errors'];
  if (is_array($errors)) {
    foreach ($errors as $element => $message) {
      form_set_error($element, $message);
    }
  }
}

/**
 * Calls for form submission service.
 */
function form_client_submit_form($form, &$form_state) {
  $service = form_client_service_load();
  $form_id = $form_state['values']['form_id'];
  $response = xmlrpc($service['url'], 'form.execute', $form_id, $form_state);
  $form_state = $response['form_state'];
}
